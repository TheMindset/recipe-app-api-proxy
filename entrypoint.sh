#!/bin/sh

# Debuging purposes
# Print failure or error on the screen
set -e 

# envsubst ==> populate environment variables in a file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Start nginx in foreground
# This way all log and output forn nginx get printed to the docker output
nginx -g 'daemon off;'

